package calculator.data;

import calculator.data.model.AddCommand;
import calculator.data.model.ApplyCommand;
import calculator.data.model.Command;
import calculator.data.model.DivideCommand;
import calculator.data.model.MultiplyCommand;
import calculator.data.model.SubtractCommand;

public class ArtmeticFactory {

	public Command createComand(String type, double value) {
		Command number = null;
		if (type.equals("apply")) {
			number = new ApplyCommand(value);
		} else if (type.equals("add")) {
			number = new AddCommand(value);
		} else if (type.equals("multiply")) {
			number = new MultiplyCommand(value);
		} else if (type.equals("divide")) {
			number = new DivideCommand(value);
		} else if (type.equals("subtract")) {
			number = new SubtractCommand(value);
		}
		return number;
	}
}
