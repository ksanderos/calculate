package calculator.data;

import java.util.ArrayList;

import calculator.data.model.Command;

public interface FileManager {
	public ArrayList<Command> getCommandList();

	public void setPathFile(String path);

}
