package calculator.data.model;

public abstract class Command implements Action {
	protected double number;

	public Command(double number) {
		this.number = number;
	}

	public Command() {
	}

	public double getNumber() {
		return number;
	}

	public void setNumber(double number) {
		this.number = number;
	}

}
