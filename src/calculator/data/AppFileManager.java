package calculator.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

import calculator.data.model.ApplyCommand;
import calculator.data.model.Command;

public class AppFileManager implements FileManager {
	private String path;
	private ArtmeticFactory artmeticFactory;

	public AppFileManager() {
		artmeticFactory = new ArtmeticFactory();
	}

	public AppFileManager(String path) {
		this.path = path;
	}

	@Override
	public ArrayList<Command> getCommandList() {
		ArrayList<Command> comandList = new ArrayList<>();
		try (Stream<String> stream = Files.lines(Paths.get(path))) {
			stream.forEach(fileLine -> addComandToList(comandList, fileLine));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return comandList;
	}

	private void addComandToList(ArrayList<Command> comandList, String fileLine) {
		Command number = convertStringToComand(fileLine);
		if (number instanceof ApplyCommand) {
			comandList.add(0, number);
		} else
			comandList.add(number);

	}

	private Command convertStringToComand(String fileLine) {
		int spaceIndex = fileLine.indexOf(" ");
		String comand = fileLine.substring(0, spaceIndex);
		String value = fileLine.substring(spaceIndex + 1, fileLine.length());
		return artmeticFactory.createComand(comand, Double.parseDouble(value));
	}

	@Override
	public void setPathFile(String path) {
		this.path = path;

	}
}
