package calculator.data.model;

public class SubtractCommand extends Command {

	public SubtractCommand(double number) {
		super(number);
	}

	@Override
	public double calculate(double input) {

		return input - number;
	}

}
