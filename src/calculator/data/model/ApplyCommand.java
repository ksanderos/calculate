package calculator.data.model;

public class ApplyCommand extends Command {

	public ApplyCommand(double number) {
		super(number);
	}

	@Override
	public double calculate(double input) {

		return number;
	}

}
