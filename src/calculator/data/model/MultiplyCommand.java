package calculator.data.model;

public class MultiplyCommand extends Command {

	public MultiplyCommand(double number) {
		super(number);
	}

	@Override
	public double calculate(double input) {

		return input * number;
	}

}
