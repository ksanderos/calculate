package calculator.data;

import java.util.ArrayList;

import calculator.data.model.Command;

public class AppDataManager implements DataManager {
	private AppFileManager appFileManager;
	private ArrayList<Command> commandList;

	public AppDataManager(String path) {
		appFileManager = new AppFileManager();
		appFileManager.setPathFile(path);
	}

	@Override
	public ArrayList<Command> getCommandList() {
		commandList = appFileManager.getCommandList();
		return commandList;
	}

	public double getCalculate() {
		double value = 0;
		for (Command number : commandList) {
			value = number.calculate(value);
		}
		return value;
	}

	@Override
	public void setPathFile(String path) {
		appFileManager.setPathFile(path);
	}

}
