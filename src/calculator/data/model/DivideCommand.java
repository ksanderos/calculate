package calculator.data.model;

public class DivideCommand extends Command {

	public DivideCommand(double number) {
		super(number);
	}

	@Override
	public double calculate(double input) {
		if (number != 0)
			return input / number;
		else {
			throw new ArithmeticException("Nie mozna dzieli� przez 0");
		}
	}

}
