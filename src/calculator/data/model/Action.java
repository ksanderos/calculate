package calculator.data.model;

public interface Action {
	public double calculate(double input);

}
