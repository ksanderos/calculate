package calculator.data.model;

public class AddCommand extends Command {

	public AddCommand(double number) {
		super(number);
	}

	@Override
	public double calculate(double input) {

		return input + number;
	}

}
